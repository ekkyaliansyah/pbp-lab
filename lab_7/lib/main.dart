import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final TextEditingController _feedbackController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFF8DB580),
          title: const Text(
            "Teman Covid",
            style: TextStyle(color: Colors.black),
          ),
          actions: [
            ElevatedButton(
                style: ElevatedButton.styleFrom(primary: const Color(0xFF8DB580)),
                onPressed: () {},
                child: const Icon(
                    Icons.list,
                    color:Colors.black
                )
            )
          ],
        ),
        body: SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 30, left: 30, right: 30),
                child: const Text(
                  "Form Feedback",
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.w900),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20, left: 30, right: 30, bottom: 20),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: const Color(0xFF8DB580), width: 2)
                ),
                child: TextFormField(
                  style: const TextStyle(fontSize: 17),
                  maxLines: null,
                  controller: _feedbackController,
                  textInputAction: TextInputAction.done,
                  decoration: const InputDecoration(
                    hintText: "Ketik di sini buat ngasih feedback :D",
                    contentPadding: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 100),
                    border: InputBorder.none,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 30, right: 30, bottom: 30),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: const Color(0xFF8DB580)),
                    child: const Text("Submit Feedback"),
                    onPressed: () {
                      if (_feedbackController.text.isNotEmpty) {
                        setState(() {
                          print(_feedbackController.text);
                          _feedbackController.text = "";
                        });
                      }
                    },
                ),
              )
            ],
          ),
        )
      ),
    );
  }
}


### 1. Apakah perbedaan antara JSON dan XML? [1]
| JSON | XML |
| ---- | --- |
| Turunan dari JavaScript | Turunan dari SGML |
| Menggunakan key-value pairs untuk menyimpan data | Menggunakan start-tag dan end-tag untuk menyimpan data |
| Relatif lebih cepat dalam hal transfer data | Relatif lebih lambat dalam hal transfer data |
| Ukuran dari documentnya relatif kecil | Ukuran dari documentnya relatif besar |
| Mudah untuk dibaca syntaxnya | Susah untuk dibaca syntaxnya |
| Support hanya strings, numbers, boolean, dan object yang bertipe data primitif | Support terhadap berbagai data type |
| Support untuk penggunaan array | Tidak support untuk penggunaan array |
| Support UTF dan ASCII encoding | Support UTF-8 dan UTF-16 encoding |

### 2. Apakah perbedaan antara HTML dan XML? [2]
| HTML | XML |
| ---- | --- |
| HyperText Markup Language | eXtensible Markup Language |
| Didesain untuk menampilkan data dan mendefine struktur tampilan website | Didesain untuk menyimpan dan mentransfer data |
| Static | Dynamic |
| Tidak case sensitive | case sensitive |
| Jumlah tag yang bisa dipakai terbatas | Jumlah tag yang bisa dipakai tidak terbatas |
| Penulisan end-tag tidak diharuskan | Wajib untuk menuliskan end-tag |
| Memperbolehkan error kecil | Tidak boleh ada error |


Referensi:    
[1] : https://hackr.io/blog/json-vs-xml    
[2] : https://www.geeksforgeeks.org/html-vs-xml/

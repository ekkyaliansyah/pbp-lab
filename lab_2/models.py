from django.db import models


class Note(models.Model):
    recipient = models.CharField(max_length=50)
    sender = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    message = models.TextField()

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFF8DB580),
          title: const Text(
              "Teman Covid",
            style: TextStyle(color: Colors.black),
          ),
          actions: [
            ElevatedButton(
                style: ElevatedButton.styleFrom(primary: const Color(0xFF8DB580)),
                onPressed: () {},
                child: const Icon(
                    Icons.list,
                    color:Colors.black
                )
            )
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            padding: const EdgeInsets.fromLTRB(0.0, 75.0, 0.0, 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Text("Teman untuk kalian",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),
                  ),
                ),
                const Center(
                  child: Text("di tengah kondisi pandemi :D",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),
                  ),
                ),
                Container(
                  height: 100,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: const [

                    ],
                  ),
                )
              ],
            ),
          )
        ),
      ),
    );
  }
}


from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)  # nama attribute
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length=10)  # nomor pokok mahasiswa attribute
    dob = models.DateField()  # date of birth attribute

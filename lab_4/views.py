from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm


@login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


@login_required(login_url='/admin/login/')
def add_note(request):
    context = {}
    form = NoteForm(request.POST, request.FILES)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')

    context['form'] = form
    return render(request, 'lab4_form.html', context)


@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)

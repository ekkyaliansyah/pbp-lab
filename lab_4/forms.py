from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):

    class Meta:
        model = Note
        fields = "__all__"
        widgets = {
            'recipient': forms.TextInput(attrs={"placeholder": "Recipient", 'class': 'form-control'}),
            'sender': forms.TextInput(attrs={"placeholder": "Sender", 'class': 'form-control'}),
            'title': forms.TextInput(attrs={"placeholder": "Title of Message", 'class': 'form-control'}),
            'message': forms.TextInput(attrs={"placeholder": "Message", 'class': 'form-control'})
        }
